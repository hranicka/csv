<?php

namespace Tests;

use Hranicka\Csv\Reader;

class ReaderTest extends \PHPUnit_Framework_TestCase
{

	public function testParse1()
	{
		$reader = new Reader(__DIR__ . '/file1.csv');

		$this->assertSame([
			[
				'id' => '1',
				'name' => 'Homer',
				'gender' => 'male',
			],
			[
				'id' => '2',
				'name' => 'Bart',
				'gender' => 'male',
			],
			[
				'id' => '4',
				'name' => 'Řeřiška',
				'gender' => 'female',
			],
		], $reader->read());
	}

	public function testParse2()
	{
		$reader = new Reader(__DIR__ . '/file1.csv', FALSE);

		$this->assertSame([
			[
				'id',
				'name',
				'gender',
			],
			[
				'1',
				'Homer',
				'male',
			],
			[
				'2',
				'Bart',
				'male',
			],
			[
				'4',
				'Řeřiška',
				'female',
			],
		], $reader->read());
	}

	public function testParseInvalid1()
	{
		$reader = new Reader(__DIR__ . '/invalid1.csv');

		$this->setExpectedExceptionRegExp('\Hranicka\Csv\InvalidFileException', '~line #2~');
		$reader->read();
	}

}
