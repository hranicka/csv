<?php

namespace Tests;

use Hranicka\Csv\Writer;
use org\bovigo\vfs;

class WriterTest extends \PHPUnit_Framework_TestCase
{

	/** @var vfs\vfsStreamDirectory */
	private $root;

	public function setUp()
	{
		$this->root = vfs\vfsStream::setup();
	}

	public function testWrite1()
	{
		$data = [
			[
				'id' => '1',
				'name' => 'Homer',
				'gender' => 'male',
			],
			[
				'id' => '2',
				'name' => 'Bart',
				'gender' => 'male',
			],
			[
				'id' => '4',
				'name' => 'Řeřiška',
				'gender' => 'female',
			],
		];

		$file = vfs\vfsStream::url('root/test.csv');
		$writer = new Writer($file, $data);
		$writer->write();

		$this->assertStringEqualsFile(__DIR__ . '/file1.csv', file_get_contents($file));
	}

	public function testWrite2()
	{
		$data = [
			[
				'id' => 1,
				'name' => 'Homer',
				'fat' => TRUE,
				'brain' => NULL,
				'data' => [1, 2, 3],
			],
		];

		$file = vfs\vfsStream::url('root/test.csv');
		$writer = new Writer($file, $data);
		$writer->write();

		$this->assertStringEqualsFile(__DIR__ . '/file2.csv', file_get_contents($file));
	}

}
